//iterables 
const firstName = "Sandhya";
for(let char of firstName){
     console.log(char);
 }

const items = ['Chocoalate', 'Cake', 'Juice'];
for(let item of items){
     console.log(item);
 }

// array like object 
 

const first_Name = "Sandhya";
console.log(first_Name.length);
console.log(first_Name[3]);


//Sets 

const numbers = new Set();
numbers.add(1);
numbers.add(2);
numbers.add(3);
numbers.add(4);
numbers.add(5);
numbers.add(6);
numbers.add(items);
if(numbers.has(3)){
     console.log("3 is present")
}else{
     console.log("3 is not present")
 }
for(let number of numbers){
     console.log(number);
}

const myArray = [2,5,3,5,7,8,4];
const uniqueElements = new Set(myArray);
let length = 0;
for(let element of uniqueElements){
    length++;
}

console.log(length);



//Maps

const person = {
     firstName : "Sandhya",
     age:22,
     1:"one"
 }
console.log(person.firstName);
console.log(person["firstName"]);
console.log(person[1]);
for(let key in person){
     console.log(typeof key);
}

// key-value pair 

const _person = new Map();
_person.set('firstName', 'Sandhya');
_person.set('age',22);
_person.set(1,'one');
_person.set([1,2,3],'onetwothree');
_person.set({1: 'one'},'onetwothree');
console.log(_person);
console.log(_person.get(1));
for(let key of _person.keys()){
     console.log(key, typeof key);
}
for(let [key, value] of _person){
     console.log(Array.isArray(key));
     console.log(key, value)
}

const person1 = {
    id: 1,
    firstName: "Sandhya"
}
const person2 = {
    id: 2,
    firstName: "Anil"
}

const extraInfo = new Map();
extraInfo.set(person1, {age: 22, gender: "female"});
extraInfo.set(person2, {age: 26, gender: "male"});

console.log(person1.id);
console.log(extraInfo.get(person1).gender);
console.log(extraInfo.get(person2).gender);



// optional chaining 

const user  = {
    firstName: "Sandhya",
    address: {houseNumber: '79357'}
}
console.log(user?.firstName);
console.log(user?.address?.houseNumber);



console.log(window);
"use strict";
function myFunc(){

     console.log(this);
 }
myFunc();


const user1 = {
    firstName : "Sandhya",
    age: 22,
    about: function(){
        console.log(this.firstName, this.age);
    }   
}

user1.about();
myFunc();
