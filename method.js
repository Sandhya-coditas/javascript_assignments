// map method 
const num = [5,8,3,9,2];
const square = function(number){
     return number*number;
 }

 const squareNumber = num.map((number, index)=>{
     return index;
});

console.log(squareNumber);

const users = [
    {firstName: "Sandhya", age: 22},
    {firstName: "Lasya", age: 22},
    {firstName: "Divya", age: 22},
    {firstName: "Shruthi", age: 22},
  ]
  
const userNames = users.map((user)=>{
    return user.firstName;
  });
  
console.log(userNames);
  
  
// filter  
  
const numbers = [11,3,6,9,12];
const evenNum = numbers.filter((number)=>{
      return number % 2 === 0;
});
console.log(evenNum);
console.log(num)


// lowToHigh
const lowToHigh = products.slice(0).sort((a,b)=>{
    return a.price-b.price
});

const highToLow = products.slice(0).sort((a,b)=>{
    return b.price-a.price;
});



users.sort((a,b)=>{
    if(a.firstName > b.firstName){
        return 1;
    }else{
        return -1;
    }
});

console.log(users);



//find 

const Array = ["bird", "fish", "dog", "turtle"];

function isLength3(string){
     return string.length === 3;
 }

const ans1 = Array.find((string)=>string.length===2);
console.log(ans1);

const myUser = users.find((user)=>user.userId===2);
console.log(myUser);



//every 


const num2 = [2,4,6,9,10];
const ans2 = num2.every((number)=>number%2===0);
console.log(ans2);


const Cart = [
    {productId: 1, productName: "mobile", price: 22000},
    {productId: 2, productName: "laptop", price: 45000},
    {productId: 3, productName: "tv", price: 27000},
]
const res1 = userCart.every((cartItem)=>cartItem.price < 30000);
console.log(ans2);



// some method 

const val = [2,7,4,9];
const ans3 = val.some((number)=>number%2===0);
console.log(ans3);


const res2 = userCart.some((cartItem)=>cartItem.price > 100000);
console.log(res2);



// fill  

const myArr = new Array(20).fill(0);
console.log(myArr);

const myArr1 = [1,2,3,4,5,6,7,8];
myArr1.fill(1,3,6);
console.log(myArr1);



// splice  
 

const _array = ['item1', 'item2', 'item3'];


const _deletedItem = myArray.splice(1, 2);// delete
console.log("delted item", _deletedItem);
 
_array.splice(1, 0,'inserted item');// insert



// insert and delete 
const deletedItem = myArray.splice(1, 2, "inserted item1", "inserted item2")
console.log("delted item", deletedItem);
console.log(myArray);
  
  
  
  