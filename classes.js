// class 
class createUser {
    constructor(firstName, lastName, email, age, address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.address = address;
    }

    about() {
        return `${this.firstName} is ${this.age} years old.`;
    }
    is18() {
        return this.age >= 18;
    }
    sing() {
        return "Ofoo Woah Woah ";
    }

}


const user1 = createUser('Sandhya', 'Kushangala', 'sandhyakushangala@gmail.com', 22, "Hyderabad");
const user2 = createUser('Lasya', 'LAskhmi', 'lakshmi@gmail.com', 22, "Rayalseema");
const user3 = createUser('Divya', 'Tamin', 'divya@gmail.com', 22, "Banagalore");
console.log(Object.getPrototypeOf(user1));



class Animal {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    eat() {
        return `${this.name} is eating`;
    }

    isSuperCute() {
        return this.age <= 1;
    }

    isCute() {
        return true;
    }
}

class Dog extends Animal {

}

const Snoopy = new Dog("Snoopy", 3);
console.log(Snoopy);
console.log(Snoopy.isCute());





//super 
class Animal1 {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    eat() {
        return `${this.name} is eating`;
    }

    isSuperCute() {
        return this.age <= 1;
    }

    isCute() {
        return true;
    }
}

class cat extends Animal1 {
    constructor(name, age, speed) {
        super(name, age);
        this.speed = speed;
    }

    run() {
        return `${this.name} is running at ${this.speed}kmph`
    }
}
const Bunny = new Dog("Bunny", 3, 45);
console.log(Bunny.run());


// same method in subclass

class turtle extends Animal {
    constructor(name, age, speed) {
        super(name, age);
        this.speed = speed;
    }

    eat() {
        return `Modified Eat : ${this.name} is eating`
    }

    run() {
        return `${this.name} is running at ${this.speed}kmph`
    }
}
const animal1 = new Animal('kurma', 2);
console.log(animal1.eat());


// getter and setters 
class Person {
    constructor(firstName, lastName, age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    get fullName() {
        return `${this.firstName} ${this.lastName}`
    }
    set fullName(fullName) {
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastName = lastName;
    }
}


const person1 = new Person("Sandy", "Kushangala", 5);
console.log(person1.fullName());
console.log(person1.fullName);
person1.fullName = "Sandhya Kushangala";
console.log(person1);


// static methods and properties
class _Person {
    constructor(firstName, lastName, age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    static classInfo() {
        return 'this is person class';
    }
    static desc = "static property";
    get fullName() {
        return `${this.firstName} ${this.lastName}`
    }
    set fullName(fullName) {
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastName = lastName;
    }
    eat() {
        return `${this.firstName} is eating`;
    }

    isSuperCute() {
        return this.age <= 1;
    }

    isCute() {
        return true;
    }
}

const _person1 = new Person("Sandy", "Kushangala", 8);
console.log(person1.eat());
const info = Person.classInfo();
console.log(person1.desc);
console.log(info);

