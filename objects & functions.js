//spread operator

const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];

const newarr = [...arr1, ...arr2, 89, 69];
const newArr = [..."123456789"];
console.log(newArr);

const obj1 = {
  key1: "value1",
  key2: "value2",
};
const obj2 = {
  key1: "valueUnique",
  key3: "value3",
  key4: "value4",
};

const newObj = { ...obj2, ...obj1, key6: "value6" };
const newObj1 = { ...["item1", "item2"] };
const newObj2 = { ..."abcdefghijklmnopqrstuvwxyz" };
console.log(newObj);

// object destructuring

const band = {
  bandName: "Rockz",
  famousSong: "My hearts a stereo",
  year: 2017,
  anotherFamousSong: "Beleiver",
};

let { bandName, famousSong, ...restProps } = band;
console.log(bandName);
console.log(restProps);


//objects in array 

const users = [
  {userId: 1,firstName: 'Sandhya', gender: 'female'},
  {userId: 2,firstName: 'Divya', gender: 'female'},
  {userId: 3,firstName: 'Lasya', gender: 'female'},
]
for(let user of users){
  console.log(user.firstName);
}


function singHappyBirthday(){
  console.log("happy birthday to you ......");
}

function sumThreeNumbers(number1, number2, number3){
  return number1 + number2 + number3;
}



// isEven
function Even(number){
    return number % 2 === 0;
 }

console.log(Even(4));


function firstChar(anyString){
     return anyString[0];
 }

console.log(firstChar("abc"));



// function 
function findTarget(array, target){
  for(let i = 0; i<array.length; i++){
      if(array[i]===target){
          return i;
      }
  }
  return -1;
}
const myArray = [2,5,7,9]
const res = findTarget(myArray, 4);
console.log(res);



//function expression 
function singHappyBirthday(){
     console.log("Happy Birthday to You ......");
 }

const singsong = function(){
  console.log("Happy Birthday to You ......");
}

singsong();

const sum3Numbers = function(number1, number2, number3){
  return number1 + number2 + number3;
}
const result = sum3Numbers(2,7,8);
console.log(result);


function isEven(number){
     return number % 2 === 0;
 }
const even = function(number){
  return number % 2 === 0;
}
console.log(even(8));

const firstChar1 = function(anyString){
  return anyString[0];
}

const findTarget1 = function(array, target){
  for(let i = 0; i<array.length; i++){
      if(array[i]===target){
          return i;
      }
  }
  return -1;
}




//block vs function scope 


if(true){
     var fname = "Sandhya";
     console.log(fname);
}

console.log(fname);


//Calling function
function myApp(){
    if(true){
        var firstName = "Sandhya";
        console.log(firstName);
    }

    if(true){
        console.log(firstName);
    }
    console.log(firstName);
}

myApp();



//Default  

function addTwo(a,b){
     if(typeof b ==="undefined"){
         b = 0;
     }
     return a+b;
 }

function addTwo(a,b=0){
  return a+b;
}

const ans1 = addTwo(8, 8);
console.log(ans1);




// other

function myFunc(a,b,...c){
     console.log("a is ${a}");
    console.log("b is ${b}");
     console.log("c is", c);
}

myFunc(7,6,3,6,8,9);

function addAll(...num){
  let total = 0;
  for(let num of num){
      total = total + num;
  }
  return total;
}

const sum = addAll(5,9,12,43,8);
console.log(sum);


// param destructuring 
const person = {
  firstName: "Sandhyarani",
  gender: "female",
  age: 22
}

function printDetails(obj){
    console.log(obj.firstName);
    console.log(obj.gender);
 }


function printDetails({firstName, gender, age}){
  console.log(firstName);
  console.log(gender);
  console.log(age);
}

printDetails(person);



// callback  

function myFunc2(name){
  console.log("inside my func 2")
  console.log(`your name is ${name}`);
}

function myFunc(callback){
  console.log("hello ")
  callback("Sandhya");
}
myFunc(myFunc2);



// function returning function 

function myFunc(){
  function hello(){
      return "hello world"
  }
  return hello;
}

const ans = myFunc();
console.log(ans());



// array methods 

const nums = [4,2,5,8];

function myFunc(number, index){
     console.log(`index is ${index} number is ${number}`);
 }

nums.forEach(function(number,index){
     console.log(`index is ${index} number is ${number}`);
 });

nums.forEach(function(number, index){
    console.log(number*3, index);
 })

const user = [
    {firstName: "Sandhya", age: 22},
    {firstName: "Lasya", age: 22},
    {firstName: "Divya", age: 22},
    {firstName: "Shruthi", age: 22},
]

user.forEach(function(user){
     console.log(user.firstName);
 });

users.forEach((user, index)=>{
   console.log(user.firstName, index);
 })

 for(let user of users){
    console.log(user.firstName);
 }



