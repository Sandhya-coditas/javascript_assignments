//Arrays 
 
let fruits = ["apple", "banana", "custardapple"];
let numbers = [1,2,3,4];
let mixed = [1,2,2.3, "string", null, undefined];
console.log(mixed);
console.log(numbers);
console.log(fruits[1]);


let obj = {};
console.log(fruits);
fruits[1] = "banana";
console.log(fruits);
console.log(typeof fruits);
console.log(typeof obj);
console.log(Array.isArray(fruits));
console.log(Array.isArray(obj));

 
fruits.push("sapota");// push
console.log(fruits);

let poppedFruit = fruits.pop();// pop 
console.log(fruits);
console.log("popped fruits is", poppedFruit);


fruits.unshift("banana");// unshift 
fruits.unshift("myfruit");
console.log(fruits);


let removedFruit = fruits.shift();// shift 
console.log(fruits);
console.log("removed fruits is ", removedFruit);



// primitve vs reference 

let num1 = 16;
let num2 = num1;
console.log("value is num1 is", num1);
console.log("value is num2 is", num2);
num1++;
console.log("after incrementing num1")
console.log("value is num1 is", num1);
console.log("value is num2 is", num2);


let array1 = ["item1", "item2"]; 
let array2 = array1;
console.log("array1", array1);
console.log("array2", array2);
array1.push("item3");
console.log("after pushing element to array 1");
console.log("array1", array1);
console.log("array2", array2);

// clone & concatenate two arrays

let arr1 = ["item1", "item2"];
//let arr2 = ["item1", "item2"];
//let arr2 = arr1.slice(0).concat(["item3", "item4"]);
//let arr2 = [].concat(arr1,["item3", "item4"]);


// spread operator
let oneMoreArray = ["item3", "item4"]
let arr2 = [...array1, ...oneMoreArray];

arr1.push("item3");

console.log(arr1===arr2);
console.log(arr1)
console.log(arr2)


// for

let fruit = ["apple", "mango", "grapes", "banana"];

 for(let i=0; i<=9;i++){
     console.log(i);
      }

 console.log(fruit.length);
 console.log(fruit[fruit.length-2]);
let fruit2 = [];
for(let i=0; i < fruit.length; i++){
    fruit2.push(fruit[i].toUpperCase());
}

console.log(fruit2);

//constant

const _fruits = ["apple", "mango"];
_fruits.push("banana");
console.log(_fruits);

// while loop 

const fruits2 = [];
let i = 0;
while(i<fruits.length){
    fruits2.push(fruits[i].toUpperCase());
    i++;
}
console.log(fruits2);

// for loop

const fruits3 = [];
for(let fruit of fruits){
     fruits3.push(fruit.toUpperCase());
 }
console.log(fruits3);

for(let i = 0; i<fruits.length; i++){
    console.log(fruits[i]);
}


// for-in loop
const fruit4 = [];

for(let index in fruits){
    fruit4.push(fruits[index].toUpperCase());
}
console.log(fruit4);



// array destructuring 
const myArray = ["abc", "def", "ghi","jkl"];
let x1 = myArray[0];
let y1 = myArray[1];
console.log("value of x1", x1);
console.log("value of y1", y1);
let [x, y, ...NewArray] = myArray;
console.log("value of x1", x1);
console.log("value of y1", y1);
console.log(NewArray);


// objects 

const person = {name:"Sandhya",age:22};
const person1 = {
    name: "Sandhya",
    age: 22,
    hobbies: ["singing", "podcasts", "watching movies"]
}
console.log(person);
console.log(person1);


console.log(person1["name"]);
console.log(person1["age"]);
console.log(person1.hobbies);

//key-value
person["person"] = "female";
console.log(person);

//dot and bracket 
const key = "email";
console.log(person["person hobbies"]);
person[key] = "sandhyakushangala@gmail.com";
console.log(person);

//Iterate object 

for(let key in person){
console.log(`${key} : ${person[key]}`);
console.log(key," : " ,person[key]);
 }

console.log(typeof (Object.keys(person)));
const val = Array.isArray((Object.keys(person)));
console.log(val);

for(let key of Object.keys(person)){
console.log(person[key]);
 }

 //computed properties

const key1 = "k1";
const key2 = "k2";

const value1 = "m1";
const value2 = "m2";

const object = {
     k1 : "m1",
     k2 : "m2",
     }

const object1 = {
    [key1] : value1,
    [key2] : value2
 }

const object2 = {};

obj[key1] = value1;
obj[key2] = value2;
console.log(obj);

