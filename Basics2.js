// And,or operator 

let firstName="Sandhya";
if(firstName[0] === "S"){
     console.log("your name starts with S")
 }
let Age=24;
 if(Age > 18){
     console.log("you are above 18");
 }

 if(firstName[0] === "S" && Age>18){
     console.log("Name starts with S and above 18");
 }else{
     console.log("inside else");
 }

 let first_Name = "andhya";
let age = 11;

if(firstName[0] === "S" || age>18){
    console.log("inside if");
}else{
    console.log("inside else");
}



// nested if else

let winningNumber = 9;
let userGuess = +prompt("Guess a number");

if(userGuess === winningNumber){
    console.log("Your guess is right!!");
}else{
    if(userGuess < winningNumber){
        console.log("too low !!!");
    }else{
        console.log("too h7igh !!!");
    }
}


// if & else if 

let temperature =10 //25//50;

if(temperature < 0){
     console.log("extremely cold outside");
 }else if(temperature < 16){
     console.log("It is cold outside");
 }else if(temperature < 25){
     console.log("wheather is okay ");
 }else if(temperature < 35){
     console.log("lets go for swim");
 }else if(temperature < 45){
     console.log("turn on AC");
 }else{
     console.log("too hot!!");
 }

 console.log("hello");


 //switch case
 
let day = 6;

switch(day){
    case 0:
        console.log("Sunday");
        break; 
    case 1:
        console.log("Monday");
        break;
    case 2:
        console.log("Tuesday");
        break;
    case 3:
        console.log("Wednesday");
        break;
    case 4:
        console.log("Thrusday");
        break;
    case 5:
        console.log("Friday");
        break;
    case 6:
        console.log("Saturday");
        break;
    default:
        console.log("Invalid Day");
}

// while loop 

let i = 0;

while(i<=5){
    console.log(i);
    i++;
}
console.log(`current value of i is ${i}`);


//while loop Example 

let num = 100;
let tot = 0; 
let a = 0;

while(a<=100){
    tot = tot + a;
     a++;
 }
console.log(tot);
let total = (num*(num+1))/2;
 console.log(tot);


 // For loop 

let c=0;
for(let c = 0;c<=9;c++){
    console.log(c);
}

console.log("value of c is ",c);


//For loop Example 

let sum1 = 0;
let total1=0;
let num1 = 100;

for(let i1 = 1; i1<=num; i1++){
    total1 = total1 + i1;
}

console.log(total1);


// break keyword

 for(let j = 1; j<=10; j++){
     if(j===4){
         break;
     }
     console.log(j);
 }

 for(let k = 1; k<=10; k++){
     if(k===4){
         continue;
     }
     console.log(k);
 }
console.log("Good Morning");


// do while loop

let m=2;
 while(m<=9){
     console.log(m);
     m++;
 }

 let n = 10;
 do{
     console.log(n);
     n++;
 }while(n<=9);

 console.log("value of n is ", n);







