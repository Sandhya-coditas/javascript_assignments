//printing statements
console.log("hello world");

"use strict";


//variables

var firstName = "Sandhya";
console.log(firstName);


//changing value 

firstName = "Rani";
console.log(firstName);


//example

var value1 = 2;
console.log(value1);


//declare variable with let keyword 

let fName = "Sandhya";
fName = "Rani";
console.log(fName);


//declare constants 

const pi = 3.14;
console.log(pi);


// String indexing  

let first_Name = "sandhyakushangala";
console.log(first_Name.length);
console.log(first_Name[first_Name.length-5]);

 
//functions

let firstname = "    Sandhya   ";

console.log(firstname.length);
firstname = firstname.trim();
console.log(firstname)
console.log(firstname.length);
firstname = firstname.toUpperCase();//changes to uppercase
firstname = firstname.toLowerCase();//changes to lowercase
console.log(firstname);

// start and end index 

let newString = firstName.slice(0,3); 
console.log(newString);


// types of operator 

let age1 = 22; 
//let firstName = "Sandhya";
console.log(typeof age1); 
age1 = age1 + "";//number to string.
console.log(typeof(age1)); "22"


let myStr = +"3"; //string to number
console.log(typeof myStr);

let _age = "20";
_age = Number(_age);
console.log(typeof _age);


//string concatenation 

let string1 = "22";
let string2 = "55";

let new_String = +string1 + +string2;
console.log(typeof new_String);


// template string 

let age=22;
let about_Me = "my name is " + firstName + " and my age is " + age; 
console.log(about_Me);
let aboutMe = `my name is ${firstName} and my age is ${age}`
console.log(aboutMe);



// undefined 

console.log(typeof firstName);
console.log(typeof firstName, firstName);

let myVariable = null;
console.log(myVariable);
myVariable = "Hyderabad";
console.log(myVariable, typeof myVariable);
console.log(typeof null);
 
let myNumber = BigInt(12);
let sameMyNumber = 123n;
console.log(myNumber);
console.log(Number.MAX_SAFE_INTEGER);
console.log(myNumber+ sameMyNumber);


//boolean & comparison operator 

let num1 = 9;
let num2 = "9";

console.log(num1<num2);

console.log(num1 === num2);

console.log(num1 !== num2);



// condition statements 

let n = 17;

if(n>=1){
     console.log("Positive number");
 }else {
    console.log("Negative number");
 }

let num = 23;

 if(num%2===0){
    console.log("even");
 }else{
     console.log("odd");
 }


 let lname= 0;

 if(lname){
     console.log(lName);
 }else{
    console.log("lastName is kinda empty");
 }


 //ternary operators 

let Age = 3;
let drink;

if(Age>=5){
     drink = "coffee";
 }else{
     drink = "milk";
 }

 console.log(drink);

//ternary operator / conditional operator 

let _Age = 3;
let Drink = _Age >= 5 ? "coffee" : "milk";
console.log(Drink);