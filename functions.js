// methods

function personInfo(){
    console.log(`person name is ${this.firstName} and age is ${this.age}`);
}

const person1 = {
    firstName : "Sandhya",
    age: 22,
    about: personInfo
}
const person2 = {
    firstName : "lasya",
    age: 22,
    about: personInfo
}
const person3 = {
    firstName : "divya",
    age: 22,
    about: personInfo
}

person1.about();
person2.about();
person3.about();


function myFunc(){

     console.log(this);
 }
myFunc();

function about(hobby, favMusician){
    console.log(this.firstName, this.age, hobby, favMusician);
}


// apply
about.apply(person1, ["waatching movies", "IlayaRaja"]);
const func = about.bind(person2, "singing", "Manisharma");
func();



// arrow functions 

const person = {
    firstName : "Sandhya",
    age: 22,
    about: () => {
        console.log(this.firstName, this.age);
    }   
}

person1.about(person1);
person1.about();



//function

function createUser(firstName, lastName, email, age, address){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    user.about = function(){
        return `${this.firstName} is ${this.age} years old.`;
    };
    user.is18 =  function(){
        return this.age >= 18;
    }
    return user;
}

const user = createUser('Sandhya', 'Kushangala', 'sandhyakushangala@gmail.com', 22, "my address");
console.log(user2);
const is18 = user2.is18();
const about = user3.about();
console.log(about);


const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    }
}

function createUser(firstName, lastName, email, age, address){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    user.about = userMethods.about;
    user.is18 = userMethods.is18;
    return user;
}

const user1 = createUser('Sandhya', 'Kushangala', 'sandhyakushangala@gmail.com',22, "Hyderabad");
const user2 = createUser('Lasya', 'LAskhmi', 'lakshmi@gmail.com', 22, "Rayalseema");
const user3 = createUser('Divya', 'Tamin', 'divya@gmail.com', 22, "Banagalore");
console.log(user1.about());
console.log(user3.about());
